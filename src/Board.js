import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Random from 'random-js';
import { Set, Stack } from 'immutable';
import { range } from 'lodash';
import { Position } from './utils';
import Field from './Field';

class Board extends Component {

  constructor(props) {
    super(props);
    this.state = {
      mines: Set(),
      uncovered: Set(),
      marked: Set(),
    };
  }

  componentDidMount() {
    this.generate(Position({ x: 0, y: 0 }), this.props.settings);
  }

  generate(initialPosition, settings) {
    console.log('Generating mines...');
    let mines = Set();
    let mt = Random.engines.mt19937();
    mt.seed(2002);
    while (settings.mineCount > mines.size) {
      const newPosition = Position({
        x: Random.integer(0, settings.width - 1)(mt),
        y: Random.integer(0, settings.height - 1)(mt),
      });
      const validPosition = !mines.add(initialPosition).has(newPosition);
      if (validPosition) {
        mines = mines.add(newPosition);
      }
    }
    this.setState({ mines });
  }

  uncover(position) {
    if (this.state.uncovered.has(position) || this.state.marked.has(position)) {
      return;
    }

    console.log('Uncover: ', position.get('x'), position.get('y'));
    if (this.state.mines.size === 0) {
      this.generate(position, this.props.settings);
    }
    const openFields = this.getOpenFields(position);
    this.setState({ uncovered: this.state.uncovered.union(openFields) });
  }

  mark(position) {
    console.log('Mark: ', position.get('x'), position.get('y'));
    const alreadyMarked = this.state.marked.has(position);
    this.setState({
      marked: alreadyMarked ? this.state.marked.delete(position) : this.state.marked.add(position)
    });
  }

  accord(position) {
    const mines = this.getMineCount(position);
    const flags = this.getFlagCount(position);

    if (mines === flags) {
      let openFields = Set();
      this.getNeighbors(position).forEach(pos => {
        if (!this.state.marked.has(pos)) {
          openFields = openFields.add(pos).union(this.getOpenFields(pos));
        }
      });
      this.setState({ uncovered: this.state.uncovered.union(openFields) });
    }
  }

  getNeighbors(position) {
    const x = position.get('x');
    const y = position.get('y');
    let neighbors = Set();
    range(-1, 2).forEach(dx => {
      range(-1, 2).forEach(dy => {
        const x1 = x + dx;
        const y1 = y + dy;
        const validPosition = x1 >= 0 && y1 >= 0 && x1 < this.props.settings.width && y1 < this.props.settings.height;
        if (validPosition)
          neighbors = neighbors.add(Position({ x: x1, y: y1 }));
      });
    });
    return neighbors.delete(position);
  }

  getMineCount(position) {
    return this.getNeighbors(position).reduce((acc, pos) => this.state.mines.has(pos) ? acc + 1 : acc, 0);
  }

  getFlagCount(position) {
    return this.getNeighbors(position).reduce((acc, pos) => this.state.marked.has(pos) ? acc + 1 : acc, 0);
  }

  getOpenFields(position) {
    let openFields = Set();
    let fields = Stack([position]);
    while (!fields.isEmpty()) {
      let currentField = fields.peek();
      fields = fields.pop();
      if (openFields.has(currentField) || this.state.marked.has(currentField)) {
        continue;
      }

      openFields = openFields.add(currentField);
      if (this.state.mines.has(currentField)) {
        return openFields;
      } else if (this.getMineCount(currentField) === 0) {
        let neighbors = this.getNeighbors(currentField);
        fields = fields.pushAll(neighbors);
      }
    }
    return openFields;
  }

  render() {
    const rows = range(0, this.props.settings.height);
    const cols = range(0, this.props.settings.width);
    return (
      <div className="App">
        <span>Mines left: {this.state.mines.size - this.state.marked.size}</span>
      {
        rows.map((y) => {
          return (
            <div key={y}>
            {
              cols.map((x) => {
                const position = Position({ x, y });
                return (
                  <Field
                    covered={!this.state.uncovered.has(position)}
                    marked={this.state.marked.has(position)}
                    mine={this.state.mines.has(position)}
                    mineCount={this.getMineCount(position)}
                    key={x}
                    uncover={() => this.uncover(position)}
                    mark={() => this.mark(position)}
                    accord={() => this.accord(position)}
                  />
                )
              })
            }
            </div>
          );
        })
      }
      </div>
    );
  }
}

Board.propTypes = {
  settings: PropTypes.shape({
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    mineCount: PropTypes.number.isRequired,
  }).isRequired,
}

export default Board;
