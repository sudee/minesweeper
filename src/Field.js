import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Field extends Component {

  constructor(props) {
    super(props);

    this.onLeftClick = this.onLeftClick.bind(this);
    this.onRightClick = this.onRightClick.bind(this);
  }

  getFieldStyle() {
    if (this.props.covered) {
      if (this.props.marked) {
        return {
          className: "fa fa-flag fa-lg",
          style: { color: "blue" },
        };
      } else {
        return {
          className: "fa",
          style: null,
        };
      }
    } else if (this.props.mine) {
      return {
        className: "fa fa-bomb fa-lg",
        style: { color: "#444" },
      };
    } else {
      return {
        className: "fa",
        style: null,
        content: this.props.mineCount === 0 ? '' : this.props.mineCount,
      };
    }
  }


  onLeftClick(event) {
    if (!this.props.marked) {
      this.props.uncover();
    }
  }

  onRightClick(event) {
    event.preventDefault()
    if (this.props.covered) {
      this.props.mark();
    } else {
      this.props.accord();
    }
  }

  render() {
    const fieldStyle = this.getFieldStyle();
    return (
      <button
        className="field"
        onClick={this.onLeftClick}
        onContextMenu={this.onRightClick}
        style={this.props.covered ? null : {borderStyle: 'solid', borderColor: '#bbb'}}>
          <i className={fieldStyle.className} style={fieldStyle.style}>{ fieldStyle.content }</i>
      </button>
    );
  }
}

Field.propTypes = {
  mine: PropTypes.bool.isRequired,
  covered: PropTypes.bool.isRequired,
};

export default Field;
