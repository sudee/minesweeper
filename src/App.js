import React, { Component } from 'react';
import './App.css';
import 'font-awesome/css/font-awesome.css';
import Board from './Board';

class App extends Component {
  render() {
    const settings = { width: 16, height: 16, mineCount: 40 };
    return (
      <div className="App">
        <Board settings={settings}/>
      </div>
    );
  }
}

export default App;
