import { Record } from 'immutable';

export const Position = Record({ x: null, y: null });
